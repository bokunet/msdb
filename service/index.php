<?php
$title="ユーザートップ | 備品管理システム";
?>
	<!DOCTYPE html>
	<html lang="ja">

	<head>
		<title><?php print $title;?></title>
		<?php include_once "../include/head.php"; ?>
		<link rel="stylesheet" href="../style/service_index.css">
	</head>

	<body>
		<header>
			<h1><?php print $title;?></h1>
		</header>
		<div id="root">
			<div id="spacer"></div>
			<div class="top menu">
				<div>
					<a href="category.php"><img src="holder.js/300x200">
						<p>貸し出し</p>
					</a>
				</div>
				<div>
					<a href="#"><img src="holder.js/300x200">
						<p>返却</p>
					</a>
				</div>
				<div>
					<a href="#"><img src="holder.js/300x200">
						<p>貸し出し状況・予約</p>
					</a>
				</div>
				<div>
					<a href="#"><img src="holder.js/300x200">
						<p>設定</p>
					</a>
				</div>
			</div>
		</div>
	</body>
	</html>