<?php
$title="借りたい備品を選択してください | 備品管理システム";
try{
	$dsn = 'mysql:dbname=test;host=localhost;charset=utf8';
	$user='root';
	$password='P@ssw0rd';
	$dbh=new PDO($dsn,$user,$password);
	$dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
	
	$sql='select * from category';
	$stmt=$dbh->prepare($sql);
	$stmt->execute();
	$dbh=null;
}
catch(Exception $e){
	print 'DBエラー : 管理者を呼ぼう。';
}
?>
	<!DOCTYPE html>
	<html lang="ja">

	<head>
		<title><?php print $title;?></title>
		<?php include_once "../include/head.php"; ?>
		<link rel="stylesheet" href="../style/category.css">
	</head>

	<body>
		<header>
			<h1><?php print $title;?></h1>
		</header>
		<div id="root">
			<div id="spacer"></div>
			<div class="category">
			<?php
				while(true){
					$rec=$stmt->fetch(PDO::FETCH_ASSOC);
					if($rec==false){
						break;
					}
					print '<div><a href="./'.$rec['category_link'].'">';
					print '<img src="holder.js/300x200">';//src="/'.$rec['category_link'].'holder.js/300x200"←本来なら
					print '<p>'.$rec['category_name'].'</p>';
					print '</a></div>';
				}
			?>
				<!--<div><a href="#"><img src="holder.js/300x200"><p>ノートPC</p></a></div>-->
			</div>
		</div>
	</body>
	</html>