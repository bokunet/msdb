<?php
	//ページ・ヘッダータイトル
	$title="備品管理システム";
	
	//カード情報受け渡し(ログイン)
	session_start();
	//$_SESSION['login']=1;
	
	//読み取りソフト起動
	//exec('felica\ReadFelica.exe');
	$fp = popen('start felica\ReadFelica.exe', 'r');
  pclose($fp);
?>
	<!DOCTYPE html>

	<html lang="ja">

	<head>
		<title><?php print $title;?></title>
		<!--全ページ共通項目は纏める-->
		<?php include_once "include/head.php"; ?>
		<!--単一css,script等-->
		<link rel="stylesheet" href="style/index.css">
		<script src="script/readtxt.js"></script>
	</head>

	<body>
		<header>
			<h1><?php print $title;?></h1>
		</header>
		<div id="root">
			<div id="spacer"></div>
			<div class="top standby">
				<video src="media/movie/standby.mp4" loop autoplay></video>
				<p>学生証を読み取り端末にタッチしてください</p>
			</div>
		</div>

		<a href="service/" id="gotop">topへ</a>
	</body>

	</html>